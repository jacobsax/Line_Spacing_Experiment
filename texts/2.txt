Malfoy couldn't believe his eyes when he saw that Harry and Ron were
still at Hogwarts the next day, looking tired but perfectly cheerful.
Indeed, by the next morning Harry and Ron thought that meeting the
three-headed dog had been an excellent adventure, and they were quite
keen to have another one. In the meantime, Harry filled Ron in about the
package that seemed to have been moved from Gringotts to Hogwarts, and
they spent a lot of time wondering what could possibly need such heavy
protection. "It's either really valuable or really dangerous," said Ron.
"Or both," said Harry.
But as all they knew for sure about the mysterious object was that it
was about two inches long, they didn't have much chance of guessing what
it was without further clues.
Neither Neville nor Hermione showed the slightest interest in what lay
underneath the dog and the trapdoor. All Neville cared about was never
going near the dog again.
Hermione was now refusing to speak to Harry and Ron, but she was such a
bossy know-it-all that they saw this as an added bonus. All they really
wanted now was a way of getting back at Malfoy, and to their great
delight, just such a thing arrived in the mail about a week later.
As the owls flooded into the Great Hall as usual, everyone's attention
was caught at once by a long, thin package carried by six large screech
owls. Harry was just as interested as everyone else to see what was in
this large parcel, and was amazed when the owls soared down and dropped
it right in front of him, knocking his bacon to the floor. They had
hardly fluttered out of the way when another owl dropped a letter on top
of the parcel.

Harry ripped open the letter first, which was lucky, because it said:
DO NOT OPEN THE PARCEL AT THE TABLE.
It contains your new Nimbus Two Thousand, but I don't want everybody
knowing you've got a broomstick or they'll all want one. Oliver Wood
will meet you tonight on the Quidditch field at seven o'clock for your
first training session.
Professor McGonagall
Harry had difficulty hiding his glee as he handed the note to Ron to
read.
"A Nimbus Two Thousand!" Ron moaned enviously. "I've never even touched
one."
They left the hall quickly, wanting to unwrap the broomstick in private
before their first class, but halfway across the entrance hall they
found the way upstairs barred by Crabbe and Goyle. Malfoy seized the
package from Harry and felt it.
"That's a broomstick," he said, throwing it back to Harry with a mixture
of jealousy and spite on his face. "You'll be in for it this time,
Potter, first years aren't allowed them."
Ron couldn't resist it.
"It's not any old broomstick," he said, "it's a Nimbus Two Thousand.
What did you say you've got at home, Malfoy, a Comet Two Sixty?" Ron
grinned at Harry. "Comets look flashy, but they're not in the same
league as the Nimbus."
"What would you know about it, Weasley, you couldn't afford half the
handle," Malfoy snapped back. "I suppose you and your brothers have to
save up twig by twig."
Before Ron could answer, Professor Flitwick appeared at Malfoy's elbow.
"Not arguing, I hope, boys?" he squeaked.
132
"Potter's been sent a broomstick, Professor," said Malfoy quickly.
"Yes, yes, that's right," said Professor Flitwick, beaming at Harry.
"Professor McGonagall told me all about the special circumstances,
Potter. And what model is it?"
"A Nimbus Two Thousand, sir" said Harry, fighting not to laugh at the
look of horror on Malfoy's face. "And it's really thanks to Malfoy here
that I've got it," he added.
Harry and Ron headed upstairs, smothering their laughter at Malfoy's
obvious rage and confusion. "Well, it's true," Harry chortled as they
reached the top of the marble staircase, "If he hadn't stolen Neville's
Remembrall I wouldn't be on the team...."
"So I suppose you think that's a reward for breaking rules?" came an
angry voice from just behind them. Hermione was stomping up the stairs,
looking disapprovingly at the package in Harry's hand.
"I thought you weren't speaking to us?" said Harry.
"Yes, don't stop now," said Ron, "it's doing us so much good."
Hermione marched away with her nose in the air.
Harry had a lot of trouble keeping his mind on his lessons that day. It
kept wandering up to the dormitory where his new broomstick was lying
under his bed, or straying off to the Quidditch field where he'd be
learning to play that night. He bolted his dinner that evening without
noticing what he was eating, and then rushed upstairs with Ron to unwrap
the Nimbus Two Thousand at last.
"Wow," Ron sighed, as the broomstick rolled onto Harry's bedspread.
Even Harry, who knew nothing about the different brooms, thought it
looked wonderful. Sleek and shiny, with a mahogany handle, it had a long
tail of neat, straight twigs and Nimbus Two Thousand written in gold
near the top.
As seven o'clock drew nearer, Harry left the castle and set off in the
dusk toward the Quidditch field. Held never been inside the stadium
before. Hundreds of seats were raised in stands around the field so that
the spectators were high enough to see what was going on.