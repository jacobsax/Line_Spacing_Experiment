
	<html>

	<body>

		<style>
		.column-left{ float: left; width: 10%; }
		.column-right{ float: right; width: 10%; }
		.column-center{ display: inline-block; width: 80%; }
		</style>
		
		<?php
		
		
			session_start();
			
			$spacings = array();
			
			if ($_SESSION["LineSpacingOrder"][0] == 1){
				$spacings[0] = "100%";
			} else if ($_SESSION["LineSpacingOrder"][0] == 2){
				$spacings[0] = "150%";
			} else {
				$spacings[0] = "200%";
			}
			
			if ($_SESSION["LineSpacingOrder"][1] == 1){
				$spacings[1] = "100%";
			} else if ($_SESSION["LineSpacingOrder"][1] == 2){
				$spacings[1] = "150%";
			} else {
				$spacings[1] = "200%";
			}
			
			if ($_SESSION["LineSpacingOrder"][2] == 1){
				$spacings[2] = "100%";
			} else if ($_SESSION["LineSpacingOrder"][2] == 2){
				$spacings[2] = "150%";
			} else {
				$spacings[2] = "200%";
			}
			
			echo "<style> p { line-height: 100%; font-size: 20px;} </style>";
			echo "<style> p1 { line-height: ".$spacings[0]."; font-size: 20px;} </style>";
			echo "<style> p2 { line-height: ".$spacings[1]."; font-size: 20px;} </style>";
			echo "<style> p3 { line-height: ".$spacings[2]."; font-size: 20px;} </style>";
			
		?>
		
		
		
		

		<div class="container">

		   <div class="column-center">
				<form action="spacing_results.php">
				
				<h3>Please read each of the text options in turn and rate their readability in the boxes provided below (with 1 being the least readable and 10 being the most readable).</h3>

				<h1>Option 1:</h1>
				<p1> When Mr. and Mrs. Dursley woke up on the dull, gray Tuesday our story starts, there was nothing about the cloudy sky outside to suggest that strange and mysterious things would soon be happening all over the country. Mr. Dursley hummed as he picked out his most boring tie for work, and Mrs. Dursley gossiped away happily as she wrestled a screaming Dudley into his high chair.</p1>
				
				
				<br>
				<h3>Please rate the readability of Option 1:</h3>
				
				<input type="text" <?php echo "name=\"option".$_SESSION["LineSpacingOrder"][0]."\""?>>
				
				<br><br>
				
				<h1>Option 2:</h1>
				<p2> When Mr. and Mrs. Dursley woke up on the dull, gray Tuesday our story starts, there was nothing about the cloudy sky outside to suggest that strange and mysterious things would soon be happening all over the country. Mr. Dursley hummed as he picked out his most boring tie for work, and Mrs. Dursley gossiped away happily as she wrestled a screaming Dudley into his high chair.</p2>
				
				
				<br>
				<h3>Please rate the readability of Option 2:</h3>
				
				<input type="text" <?php echo "name=\"option".$_SESSION["LineSpacingOrder"][1]."\""?>>
				
				
				<br><br>
				
				<h1>Option 3:</h1>
				<p3> When Mr. and Mrs. Dursley woke up on the dull, gray Tuesday our story starts, there was nothing about the cloudy sky outside to suggest that strange and mysterious things would soon be happening all over the country. Mr. Dursley hummed as he picked out his most boring tie for work, and Mrs. Dursley gossiped away happily as she wrestled a screaming Dudley into his high chair.</p3>
				
				
				
				<br>
				<h3>Please rate the readability of Option 3:</h3>
				
				<input type="text" <?php echo "name=\"option".$_SESSION["LineSpacingOrder"][2]."\""?>>
				
				<br><br>
				
									
					
				<input type="submit">
				</form>

			</div>
			<div class="column-left"> 
			   	 <br>
				 </div>
			   <div class="column-right"> <br> </div>
			</div>
		</div>
	</body>
</html>
