<?php
	include 'functions.php';
	session_start();
	
	$current_text = $_SESSION["TextOrder"][$_SESSION["TextPosition"]];

	
	if ($_SESSION["SurveyPosition"] == -1){ //called before the text has been displayed
		$_SESSION["SurveyPosition"] = 0; //move forward one stage in the program
		$_SESSION["Data"][$current_text] = array();
		$_SESSION["Data"][$current_text]["Survey"] = array();
		$_SESSION["start_time"] = millis();
		$_SESSION["Data"][$current_text]["LineSpacing"] = $_SESSION["LineSpacingOrder"][$_SESSION["TextPosition"]];
		
		header( 'Location: display_text.php?text='.$current_text.'&spacing='.$_SESSION["Data"][$current_text]["LineSpacing"]);
		
	} else if ($_SESSION["SurveyPosition"] == 0){ //called after the text has been displayed
		$_SESSION["Data"][$current_text]["ReadTime"] = millis() - $_SESSION["start_time"];
		$_SESSION["SurveyPosition"] = 1; //move forward one stage in the program
		
		$_SESSION["start_time"] = millis();
		
		header( 'Location: display_survey.php?text='.$current_text."&question=".($_SESSION["SurveyPosition"])); //start the survey
		
	} else if ($_SESSION["SurveyPosition"] == $_SESSION["QuestionsPerSurvey"] + 1){ //if we have finished the survey
	
		
	
		if ($_SESSION["TextPosition"] < 2){ //if we have not gone through all the texts, reset for the next text
			$_SESSION["TextPosition"] = $_SESSION["TextPosition"] + 1;
			$_SESSION["SurveyPosition"] = -1; 
			
			header( 'Location: controller.php');

		} else { //if we have gone through all the texts, write a csv file output
			$csvfile = fopen("results.csv", "a");
			$text = "";
			
			$text = $text.$_SESSION["UserID"];
			$text = $text.",";
			
			for ($i = 1; $i <= 3; $i++){ //loop through the texts from 1 to 3, adding the text read time and line spacing
				$text = $text.$_SESSION["Data"][$i]["ReadTime"];
				$text = $text.",";
				$text = $text.$_SESSION["Data"][$i]["LineSpacing"];
				$text = $text.",";
			
			}
						
			for ($i = 1; $i <= 3; $i++){ //add the question results and answer time
				for ($j = 1; $j < $_SESSION["QuestionsPerSurvey"] + 1; $j++){
					
					$text = $text.$_SESSION["Data"][$i]["Survey"][$j]["Answer"];
					$text = $text.",";
					$text = $text.$_SESSION["Data"][$i]["Survey"][$j]["Time"];
					$text = $text.",";
					
				}
			}
			
			for ($i = 1; $i <= 3; $i++){
				$text = $text.$_SESSION["Data"]["Spacing_Preference"][$i].",";
			}
			
			$text = substr($text, 0, -1); //strip the redundant final comma
			
			fwrite($csvfile, $text); //write out to the csv file
			fwrite($csvfile, "\r\n");
			fclose($csvfile);
			
			//echo "Thank you for compleing the survey.";
			header ( 'Location: http://goo.gl/forms/TwHUN1J1Xk');
			
		}
	} else { //called whilst the survey is being answered
		$_SESSION["Data"][$current_text]["Survey"][$_SESSION["SurveyPosition"]] = array(); //create a new entry in the data dictionary
		
		//echo "answer = ".trim($_POST["answer"]);
		//echo "selection = ".trim($_POST["selection"]);
		
		if (trim($_POST["answer"]) == trim($_POST["selection"])){ //check to see if the answer is correct or not
			$_SESSION["Data"][$current_text]["Survey"][$_SESSION["SurveyPosition"]]["Answer"] = 1;
			//echo "correct choice";
		} else {
			$_SESSION["Data"][$current_text]["Survey"][$_SESSION["SurveyPosition"]]["Answer"] = 0;
		}
		$_SESSION["Data"][$current_text]["Survey"][$_SESSION["SurveyPosition"]]["Time"] = millis() - $_SESSION["start_time"];
		
		$_SESSION["SurveyPosition"] = $_SESSION["SurveyPosition"] + 1; //increment the study position
		
		if ($_SESSION["SurveyPosition"] <= $_SESSION["QuestionsPerSurvey"]){
			$_SESSION["start_time"] = millis();
			
			//echo '<a href = "display_survey.php?text='.$current_text."&question=".($_SESSION["SurveyPosition"]).'">continue</a>';
			header( 'Location: display_survey.php?text='.$current_text."&question=".($_SESSION["SurveyPosition"]));
		} else {
			header( 'Location: controller.php');
		}
	}
?>